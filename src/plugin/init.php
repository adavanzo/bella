<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/string/path.php';

function bella_plugin_init(bella_app $app, bella_struct_cms $cms, string $name, array $plugin_args = []): void
{
  $initme = sesto_path($app->config['plugin_dir'], $name, 'initme.php');
  if (is_file($initme) && is_readable($initme)) {
    require_once $initme;
    if (isset($config) && is_array($config)) {
      /* add pluging views on cms */
      foreach ($config['views'] ?? [] as $name => $file) {
        $cms->views[$name] = $file;
      }
    }
  }
}
