<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once BELLA_DIR . '/plugin/init.php';
require_once BELLA_DIR . '/struct/app.php';
require_once BELLA_DIR . '/struct/cms.php';

function bella_plugin_initall(bella_app $app, bella_struct_cms $cms): void
{
  /* init plugins */
  foreach ($app->config['plugins'] as $name => $data) {
    bella_plugin_init($app, $cms, $name, is_array($data) ? $data : []);
  }
}
