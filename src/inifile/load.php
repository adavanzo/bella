<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

function bella_inifile_load(string $filename, bool $process_sections = true, int $scanner_mode = INI_SCANNER_TYPED): array
{
  $config = [];
  if (is_file($filename) && is_readable($filename)) {
    $config = parse_ini_file($filename, $process_sections, $scanner_mode) ?: [];

  }
  return $config;
}
