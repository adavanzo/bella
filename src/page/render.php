<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/view/render.php';
require_once SESTO_DIR . '/html/build.php';

require_once BELLA_DIR . '/cms/init.php';
require_once BELLA_DIR . '/view/render.php';
require_once BELLA_DIR . '/processor/add.php';

function bella_cms_text_path(string $path, string $extension)
{
  $text_path = $path;
  if ($extension !== '') {
    $text_path .= '.' .  $extension;
  }
  return $text_path;
}

function bella_page_render(bella_app $app)
{
  /* start session */
  session_start();

  /* $cms init */
  $cms = bella_cms_init($app);

//  sesto_d($cms, '$cms');

  /* content management */
  /* read the text */

  if ($cms->page->type === 'view') {
    $cms->content_file = $app->filename . '.phtml';
    $cms->views['page_html'] = $cms->content_file;
  } else {
    foreach ($app->processors as $extension => $scd) {
      $cms->content_file = $app->filename . $extension;
      if (is_file($cms->content_file) && is_readable($cms->content_file)) {
        $cms->page->html = $scd->call($app, $cms);
      }
    }
  }
//  sesto_d($cms, '$cms');
//  die;
//  sesto_d($cms, '$cms');
//  die;
//  if ($page_content_ext !== '') {
//    $page_text_file .= '.' .  $page_content_ext;
//  }
//  if (is_file($page_text_file) && is_readable($page_text_file)) {
//    $text = file_get_contents($page_text_file);
//    if (is_string($text)) {
//      $cms->page->text = $text;
//    } else {
//      throw new exception('Text not readable', 500);
//    }
//  } else {
//    throw new exception('Text not found', 404);
//  }
//  sesto_hook_simple::getme()->procedure('bella.page.text.retrieve.post', $app, $cms);

  /* process the text into html */
//  $cms->page->html =  sesto_hook_simple::getme()->function('bella.processor.process', $app, $cms);
//  sesto_hook_simple::getme()->procedure('bella.page.html.post', $app, $cms);

  /* inline plugins */
  $matches = [];
//  // ^({plugin:)((\[[^\}]{3,})?\{s*[^\}\{]{3,}?:.*\}([^\{]+\])?)(})
  preg_match_all('/({bella_json:)(.*)(})/', $cms->page->html, $matches);
//  sesto_d($matches, '$matches');
  $string_to_replace = $matches[0][0] ?? '';
//  sesto_d($cms->page->html, '$cms->page->html');
  if (isset($matches[2][0])) {
    $data = json_decode($matches[2][0]);
    $plugin_name = $data->plugin ?? null;
    $plugin_args = (array) ($data->args ?? []);
//    sesto_d($plugin_name, '$plugin_name');
//    sesto_d($plugin_args, '$plugin_args');
//    $call = $data->plugin;
//    sesto_d($data, '$data');
    if (is_string($plugin_name) && is_array($plugin_args)) {
      sesto_hook_simple::getme()->procedure('bella.page.html.replace', $app, $cms);
    }
  }

//  sesto_d($cms, '$cms');
//  sesto_d($app, '$app');
//  die;
  /* render */
  bella_view_render($app, $cms);
//  sesto_d(sesto_registry());

//  sesto_d($cms->metadata, '$cms->metadata');
}
