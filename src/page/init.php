<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once BELLA_DIR . '/config/page.php';
require_once BELLA_DIR . '/plugin/initall.php';
require_once BELLA_DIR . '/struct/page.php';

function bella_page_init(bella_app $app, bella_struct_cms $cms): void
{
  $config = bella_config_page($app->filename);

  $cms->page = new bella_struct_page($config);

  /* init plugins */
//  bella_plugin_initall($app, $cms);

  /* init views */
  foreach ($config['views'] ?? [] as $name => $filename) {
//    if (dirname($filename) === '.') {
//      $filename = sesto_path($app->dirname, $filename);
//    }
    $cms->views[$name] = $filename;
  }

//  sesto_d($config);

}
