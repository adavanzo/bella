<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once BELLA_DIR . '/config/folder.php';
require_once BELLA_DIR . '/struct/path.php';
require_once BELLA_DIR . '/struct/segment.php';
require_once BELLA_DIR . '/plugin/initall.php';

function bella_path_init(bella_app $app, bella_struct_cms $cms): void
{
  $cms->path = new bella_struct_path();
  $url_path = '/';
  $parts = explode('/', $app->route->dirname);
  if ('/' === $app->route->dirname) {
    unset($parts[1]);
  }
  foreach ($parts as $key => $dir) {
    if ('' === $dir) {
      $dirname = $app->content_dir;
    } else {
      $url_path .= $dir . '/';
      $dirname .= DIRECTORY_SEPARATOR . $dir;
    }

    $config = bella_config_folder($dirname);
    $segment = new bella_struct_segment($config);
    $segment->dirname = $dirname;
    $segment->url = $app->config['site_host'] . $url_path;

    $cms->path->items[$key] = $segment;
    $cms->path->key_last = $key;

    /* init plugins */
//    bella_plugin_initall($app, $cms);

    /* init views */
    foreach ($config['views'] ?? [] as $name => $filename) {
      if ($filename === '_default') {
        $cms->views[$name] = $cms->template[$name];
      } else {
        $cms->views[$name] = $filename;
      }
    }

//    sesto_d($cms->path);
//    foreach ($segment->views as $name -> $cms->path) {
//      $cms->views[$name] = $cms->path;
//    }
  }

}
