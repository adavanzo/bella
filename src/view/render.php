<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

function bella_view_render(bella_app $app, bella_struct_cms $cms): void
{
    ob_start();
    sesto_view_render($cms->views, 'layout', $cms);
    $app->response = ob_get_contents();
    ob_end_clean();

//    sesto_d(sesto_hook_simple::getme()->get());
//    die;

    $app->response = sesto_hook_simple::getme()->filter('bella.output.replace', $app->response, $app, $cms);
//    if (is_string($output)) {
//      $cms->output = $output;
//    }
    echo $app->response;
}
