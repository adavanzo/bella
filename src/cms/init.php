<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/string/path.php';
require_once SESTO_DIR . '/util/config.php';
require_once BELLA_DIR . '/struct/cms.php';
require_once BELLA_DIR . '/struct/thing.php';
require_once BELLA_DIR . '/site/init.php';
require_once BELLA_DIR . '/metadata/init.php';
require_once BELLA_DIR . '/path/init.php';
require_once BELLA_DIR . '/page/init.php';
require_once BELLA_DIR . '/plugin/initall.php';

function bella_cms_init(bella_app $app): bella_struct_cms
{
  $template_config = sesto_config(sesto_path($app->template_dir, '_config')) ?: [];
  $cms = new bella_struct_cms(['template' => $template_config, 'views' => $template_config]);
  bella_plugin_initall($app, $cms);
  bella_site_init($app, $cms);
  bella_metadata_init($app, $cms);
  bella_path_init($app, $cms);
  bella_page_init($app, $cms);

  sesto_hook_simple::getme()->procedure('bella.cms.init.post', $app, $cms);
  sesto_hook_simple::getme()->procedure('bella.metadata.init.post', $app, $cms);
  sesto_hook_simple::getme()->procedure('bella.path.init.post', $app, $cms);
  sesto_hook_simple::getme()->procedure('bella.page.init.post', $app, $cms);

  return $cms;
}
