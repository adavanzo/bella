<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once BELLA_DIR . '/struct/site.php';

function bella_site_init(bella_app $app, bella_struct_cms $cms): void
{
  $cms->site = new bella_struct_site();
  $cms->site->host = $app->config['site_host'];
  $cms->site->name = $app->config['site_name'];
}
