<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

function bella_processor_add(bella_app $app, string $extension, sesto_scd $scd): void
{
  $app->processors[$extension] = $scd;
}
