<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once BELLA_DIR . '/inifile/load.php';

function bella_config_folder(string $dirname): array
{
  return  bella_inifile_load($dirname . DIRECTORY_SEPARATOR . '_config.ini');
}
