<?php

/* =============================================================================
 * Bella CMS - Copyright (c) Andrea Davanzo - License MPL v2.0 - bellacms.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/util/config.php';

function bella_config_page(string $filename): array
{
  $ini_file = $filename . '.ini';
  $ini_data = [];
  if (is_file($ini_file) && is_readable($ini_file)) {
    $ini_data = @parse_ini_file($ini_file ,true, INI_SCANNER_TYPED) ?: [];
  }
  return  $ini_data;
}
