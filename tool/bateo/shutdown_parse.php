<?php
/* =============================================================================
 * Naranza Bateo, Copyright (c) Andrea Davanzo, License GNU GPL v3.0, bateo.dev
 * ========================================================================== */

declare(strict_types = 1);

function bateo_shutdown_parse(string $path, string $destination): void
{
  if (!is_file($path) && !is_readable($path)) {
    return;
  }
  $handle = fopen($path, 'r');
  if (!$handle) {
    return;
  }
  echo "== Shutdown summary\n";
  while (false !== ($line = fgets($handle))) {
    $error = json_decode(str_replace("\n", "", $line), true);
    echo "\n";
    $error['file'] = substr($error['file'], strpos($error['file'], $destination));
    echo sprintf("File: %s\n", $error['file']);
    echo sprintf("Type: %d\n", $error['type']);
    echo sprintf("Line: %d\n", $error['line']);
    echo sprintf("Message: %s\n", $error['message']);
  }
  fclose($handle);
}
