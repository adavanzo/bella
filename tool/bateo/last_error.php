<?php
/* =============================================================================
 * Naranza Bateo, Copyright (c) Andrea Davanzo, License GNU GPL v3.0, bateo.dev
 * ========================================================================== */

declare(strict_types=1);

function bateo_last_error(string $path)
{
  /**
   * The following error types cannot be handled with a user defined function:
   * E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR,
   * E_COMPILE_WARNING independent of where they were raised, and most of
   * E_STRICT raised in the file where set_error_handler() is called.
   *
   * https://www.php.net/manual/en/function.set-error-handler.php
   */
  $shutdown_errors = [E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING, E_STRICT];
  $e = error_get_last();
  if (null !== $e && in_array($e['type'], $shutdown_errors)) {
    $encoded = json_encode($e);
    file_put_contents($path, $encoded . "\n", FILE_APPEND);
  }
}
