<?php
/* =============================================================================
 * Naranza Bateo, Copyright (c) Andrea Davanzo, License GNU GPL v3.0, bateo.dev
 * ========================================================================== */

declare(strict_types = 1);

const BATEO_DIR = __DIR__;
const BATEO_VERSION = '0.6.3';
const BATEO_CODENAME = 'Monteverdi';
const BATEO_TEST_PASS = 0;
const BATEO_TEST_FAIL = 1;
const BATEO_TEST_UNDEFINED = 2;
const BATEO_TEST_ERROR = 3;
const BATEO_TEST_HALT = 4;
const BATEO_TEST_SKIP = 5;
